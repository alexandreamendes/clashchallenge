angular
  .module('ClashChallenge', ['ionic'])
  .controller('DesafioController', DesafioController)

  function DesafioController($http, $ionicPopup, $ionicLoading) {

  var vmDesafio = this;

  vmDesafio.ListaMembrosDesafio = "";
  vmDesafio.InformacoesClaDesafio = "";


  vmDesafio.PesquisarDesafio = function () {

    $http.get("http://www.clashchallenge.com.br/apiClashChallenge/ConsultainfclaMembros?TagCla=%239C9YJGG").then(ConsultainfclaMembrosDesafio, ConsultainfclaMembrosErrorDesafio)

    function ConsultainfclaMembrosDesafio(data) {
      $ionicLoading.hide();
      vmDesafio.ListaMembrosDesafio = data.data.items;

      if (vmDesafio.ListaMembrosDesafio == "") {
        var alertPopup = $ionicPopup.alert({
          title: 'Ops!',
          template: 'Clã não encontrado!',
          cssClass: 'msgPopup font-clash'
        });
      }
    }

    function ConsultainfclaMembrosErrorDesafio(data) {
      $ionicLoading.hide();
    }
  }
}