angular
  .module('ClashChallenge', ['ionic'])
  .controller('MeuClaController', MeuClaController)

function MeuClaController($http, $ionicPopup, $ionicLoading) {

  var vmMeuCla = this;

  vmMeuCla.TelaCla = {
    TagCla: ""
  }

  vmMeuCla.ListaMembros = "";
  vmMeuCla.InformacoesCla = "";


  // var _urlimgMeuCla = 'https://besthqwallpapers.com/Uploads/21-1-2017/12587/thumb2-clash-of-clans-characters-strategy-vikings.jpg'

  // $(window).resize(function () {
  //   CarregaImagemDivAlturaDinamicaMeuCla($("#searchCla"), _urlimgMeuCla);
  // });

  // $(document).ready(function () {
  //   CarregaImagemDivAlturaDinamicaMeuCla($("#searchCla"), _urlimgMeuCla);
  // });


  // vmMeuCla.PesquisarCla = function (data) {
    $ionicLoading.show({
      template: 'Aguarde...',
      cssClass: 'msgPopup'

    })

    _claTag = "#9C9YJGG";

    $http.get("http://www.clashchallenge.com.br/apiClashChallenge/Consultainfcla?TagCla=" + _claTag.replace("#", "%23")).then(Consultainfcla, ConsultainfclaError)

    $http.get("http://www.clashchallenge.com.br/apiClashChallenge/ConsultainfclaMembros?TagCla=" + _claTag.replace("#", "%23")).then(ConsultainfclaMembros, ConsultainfclaMembrosError)

    function Consultainfcla(data) {
      vmMeuCla.InformacoesCla = data.data.items;

      if (vmMeuCla.InformacoesCla == "") {
        vmMeuCla.ListaMembros = "";
        var alertPopup = $ionicPopup.alert({
          title: 'Ops!',
          template: 'Clã não encontrado!',
          cssClass: 'msgPopup'
        });
      }

    }

    function ConsultainfclaError(data) {
      $ionicLoading.hide();
      var alertPopup = $ionicPopup.alert({
        title: 'Ops!',
        template: 'Clã não encontrado!',
        cssClass: 'msgPopup'
      });
    }

    function ConsultainfclaMembros(data) {
      $ionicLoading.hide();
      vmMeuCla.ListaMembros = data.data.items;

      if (vmMeuCla.ListaMembros == "") {
        var alertPopup = $ionicPopup.alert({
          title: 'Ops!',
          template: 'Clã não encontrado!',
          cssClass: 'msgPopup font-clash'
        });
      }
    }

    function ConsultainfclaMembrosError(data) {
      $ionicLoading.hide();
      // var alertPopup = $ionicPopup.alert({
      //   title: 'Ops!',
      //   template: 'Clã não encontrado!',
      //   cssClass: 'msgPopup font-clash'
      // });
    }

  

  function CarregaImagemDivAlturaDinamicaMeuCla(div, url) {
    div.addClass('divDinan');
    var img = new Image();
    img.onload = function () {

      var aspectRatio = img.width / img.height;
      var width = div.width();
      var height = 100;

      div.height(height);
      div.css('background-image', 'url(' + url + ')');
    };
    img.src = url;
  }
}