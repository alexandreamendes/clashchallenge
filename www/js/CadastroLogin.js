angular
    .module('ClashChallenge', ['ionic'])
    .controller('CadastroLoginController', CadastroLoginController)

function CadastroLoginController($http, $ionicPopup, $ionicLoading) {

    var vmCadastroLogin = this;

    vmCadastroLogin.Cadastro = {
        email: "",
        TagMember: "",
        password: ""
    }

    vmCadastroLogin.OnCadastroLogin = function (data) {

        var config = {
            headers: {
                'email': data.email,
                'TagMember': data.TagMember,
                'password': data.password
            }
        };

        if ((data.TagMember != "") && (data.password != "")) {
            $http.post("http://www.clashchallenge.com.br/apiClashChallenge/InserirLogin", data, config).then(HttPostSucces, HttpPostError)
        }
        else {
            if (data.email == "") {
                var alertPopup = $ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Obrigatório inserir o email!',
                    cssClass: 'msgPopup'
                });
            }
            else if ((data.TagMember == "") && ((data.email != ""))) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Obrigatório inserir o tag Member(login)!',
                    cssClass: 'msgPopup'
                });
            }
            else if ((data.password == "") && (data.TagMember != "") && (data.email != "")) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Obrigatório inserir a senha!',
                    cssClass: 'msgPopup'
                });
            }
        }
    }

    function HttPostSucces(data) {
         var alertPopup = $ionicPopup.alert({
                    title: 'Ops!',
                    template: data.data.Message,
                    cssClass: 'msgPopup'
                });
    }

    function HttpPostError(data) {
                 var alertPopup = $ionicPopup.alert({
                    title: 'Ops!',
                    template: data.data.Message,
                    cssClass: 'msgPopup'
                });
    }

    // alert('entrou');

}