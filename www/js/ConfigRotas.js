angular
  .module('ClashChallenge', ['ionic'])
  .controller('MeuClaController', MeuClaController)
  .controller('DesafioController', DesafioController)
  .controller('CriarDesafioController', CriarDesafioController)
  .controller('LoginController', LoginController)
  .controller('CadastroLoginController', CadastroLoginController)
  .controller('RecuperarSenhaController', RecuperarSenhaController)
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('botton')
    $stateProvider
      .state('Tabs',
      {
        url: '/Tabs',
        abstract: true,
        templateUrl: 'pages/Tabs.html'
      })
      .state('Tabs.MeuCla',
      {
        url: '/MeuCla',
        views: {
          'MeuCla': {
            templateUrl: 'pages/MeuCla.html',
            controller: 'MeuClaController'
          }
        }
      })
      .state('Tabs.Desafios',
      {
        url: '/Desafios',
        views: {
          'Desafios': {
            templateUrl: 'pages/Desafios.html',
            controller: 'DesafioController'
          }
        }
      })
      .state('Tabs.CriarDesafio',
      {
        url: '/CriarDesafio',
        views: {
          'CriarDesafio': {
            templateUrl: 'pages/CriarDesafio.html',
            controller: 'CriarDesafioController'
          }
        }
      })
    $urlRouterProvider.otherwise('/Tabs/MeuCla');
  })

  
      // .state('CadastroLogin',
      // {
      //   url: '/CadastroLogin',
      //   templateUrl: 'pages/CadastroLogin.html',
      //   controller: 'CadastroLoginController'

      // })
      // .state('RecuperarSenha',
      // {
      //   url: '/RecuperarSenha',
      //   templateUrl: 'pages/RecuperarSenha.html',
      //   controller: 'RecuperarSenhaController'

      // })
        // .state('Login',
      // {
      //   url: '/Login',
      //   templateUrl: 'pages/Login.html',
      //   controller: 'LoginController'
      // })
