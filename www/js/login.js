angular
  .module('ClashChallenge', ['ionic'])
  .controller('LoginController', LoginController)

function LoginController($http, $ionicPopup, $ionicLoading) {

  var vmLogin = this;

  vmLogin.Login = {
    TagMember: "",
    password: ""
  }

  vmLogin.OnLogin = function (data) {

    var config = {
      headers: {
        'TagMember': data.TagMember,
        'password': data.password
      }
    };

    if ((data.TagMember != "") && (data.password != "")) {
      $ionicLoading.show({
        template: 'Aguarde...',
        cssClass: 'msgPopup'

      })
      $http.post("http://www.clashchallenge.com.br/apiClashChallenge/Logando", data, config).then(HttPostSucces, HttpPostError)
    }
    else {
      if (data.TagMember == "") {
        $ionicPopup.alert({
          title: 'Ops!',
          template: 'Obrigatório inserir o login!',
          cssClass: 'msgPopup'
        });

      }

      if ((data.password == "") && (data.TagMember != "")) {
        var alertPopup = $ionicPopup.alert({
          title: 'Ops!',
          template: 'Obrigatório inserir a senha!',
          cssClass: 'msgPopup'
        });

      }
    }
  }

  function HttPostSucces(data) {
    $ionicLoading.hide();
    var alertPopup = $ionicPopup.alert({
      title: 'Ops!',
      template: data.data.Message,
      cssClass: 'msgPopup'
    });

  }

  function HttpPostError(data) {
    $ionicLoading.hide();
    var alertPopup = $ionicPopup.alert({
      title: 'Ops!',
      template: data.data.Message,
      cssClass: 'msgPopup'
    });
  }


}