angular
    .module('ClashChallenge', ['ionic'])
    .controller('RecuperarSenhaController', RecuperarSenhaController)

function RecuperarSenhaController($http, $ionicPopup, $ionicLoading) {

    var vmRecuperarSenha = this;

    vmRecuperarSenha.Recuperar = {
        email: "",
        TagMember: ""

    }

    vmRecuperarSenha.OnRecuperarSenha = function (data) {

        var config = {
            headers: {
                'email': data.email,
                'TagMember': data.TagMember
            }
        };

        if ((data.TagMember != "") && (data.password != "")) {
            $ionicLoading.show({
                template: 'Enviando e-mail',
                cssClass: 'msgPopup'

            })

            alert('Chegou');
            // $http.post("http://www.clashchallenge.com.br/apiClashChallenge/Logando", data, config).then(HttPostSucces, HttpPostError)
        }
        else {
            if (data.email == "") {
                $ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Obrigatório inserir o email!',
                    cssClass: 'msgPopup'
                });

            }

            if ((data.password == "") && (data.TagMember != "")) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Obrigatório inserir a tag Member!',
                    cssClass: 'msgPopup'
                });

            }
        }
    }

    function HttPostSucces(data) {
        alert(data.data.Message);
        $ionicLoading.hide();

    }

    function HttpPostError(data) {
        $ionicLoading.show({
            template: 'Aguarde...',
            cssClass: 'msgPopup'

        })
        alert(data.data.Message);
        $ionicLoading.hide();
    }


}