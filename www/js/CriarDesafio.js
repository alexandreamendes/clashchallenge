angular
  .module('ClashChallenge', ['ionic'])
  .controller('CriarDesafioController', CriarDesafioController)

function CriarDesafioController($http, $ionicPopup, $ionicLoading,$scope) {

  var vmCriarDesafio = this;

  var _claTag = "#9C9YJGG";

  vmCriarDesafio.ListaMembrosCriarDesafio = "";
  vmCriarDesafio.InformacoesClaCriarDesafio = "";

  var options = $.extend(
    {},                                  // empty object
    $.datepicker.regional["pt-BR"],         // fr regional
    { minDate: 0 , 
      maxDate : "+1M" /*, ... */ } // your custom options
);
$("#datepicker").datepicker(options);

  //  $("#datepicker").datepicker({minDate : 0 ,maxDate : "+1M"});  
  //  $("#datepicker").datepicker($.datepicker.regional["pt-BR"]);  
   
  $http.get("http://www.clashchallenge.com.br/apiClashChallenge/Consultainfcla?TagCla=" + _claTag.replace("#", "%23")).then(ConsultainfclaCriarDesafio, ConsultainfclaErroCriarDesafio)
  $http.get("http://www.clashchallenge.com.br/apiClashChallenge/ConsultainfclaMembros?TagCla=" + _claTag.replace("#", "%23")).then(ConsultainfclaMembrosCriarDesafio, ConsultainfclaMembrosErrorCriarDesafio)

  function ConsultainfclaCriarDesafio(data) {
    vmCriarDesafio.InformacoesClaCriarDesafio = data.data.items;

    if (vmCriarDesafio.InformacoesClaCriarDesafio == "") {
      vmCriarDesafio.ListaMembrosCriarDesafio = "";
      var alertPopup = $ionicPopup.alert({
        title: 'Ops!',
        template: 'Clã não encontrado!',
        cssClass: 'msgPopup'
      });
    }

  }

  function ConsultainfclaErroCriarDesafio(data) {
    $ionicLoading.hide();
    var alertPopup = $ionicPopup.alert({
      title: 'Ops!',
      template: 'Clã não encontrado!',
      cssClass: 'msgPopup'
    });
  }

  function ConsultainfclaMembrosCriarDesafio(data) {
    $ionicLoading.hide();
    vmCriarDesafio.ListaMembrosCriarDesafio = data.data.items;

    if (vmCriarDesafio.ListaMembrosCriarDesafio == "") {
      var alertPopup = $ionicPopup.alert({
        title: 'Ops!',
        template: 'Clã não encontrado!',
        cssClass: 'msgPopup font-clash'
      });
    }
  }

  function ConsultainfclaMembrosErrorCriarDesafio(data) {
    $ionicLoading.hide();
  }

  $scope.mytime = new Date();

  $scope.hstep = 1;
  $scope.mstep = 15;

  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };

  $scope.ismeridian = true;
  $scope.toggleMode = function() {
    $scope.ismeridian = ! $scope.ismeridian;
  };

  $scope.update = function() {
    var d = new Date();
    d.setHours( 14 );
    d.setMinutes( 0 );
    $scope.mytime = d;
  };

  $scope.changed = function () {
    $log.log('Time changed to: ' + $scope.mytime);
  };

  $scope.clear = function() {
    $scope.mytime = null;
  };
}